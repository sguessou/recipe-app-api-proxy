#!/bin/sh

set -e

envsubst < /etc/nginx/default.conf.tpl > /etc/nginxd/conf.d/default.conf

nginx -g 'daemon off;'
